/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function () {
        this.bindEvents();
    },
    globalObjects: {

    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener("")
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        // listeningElement.setAttribute('style', 'display:none;');
        // receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};





var globalObjects = {};

document.addEventListener("DOMContentLoaded", function () {
    
    document.querySelector("#createOrg").addEventListener("click", function () {

        cordova.plugins.barcodeScanner.scan(
            function (result) {
                addNewOrg(result.text);

            },
            function (error) {
                alert("Scanning failed: " + error);
            },
            {
                preferFrontCamera: false, // iOS and Android
                showFlipCameraButton: false, // iOS and Android
                showTorchButton: false, // iOS and Android
                torchOn: false, // Android, launch with the torch switched on (if available)
                saveHistory: true, // Android, save scan history (default false)
                prompt: "Place the QR  inside the scan area", // Android
                resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
                formats: "QR_CODE", // default: all but PDF_417 and RSS_EXPANDED
                orientation: "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
                disableAnimations: true, // iOS
                disableSuccessBeep: false // iOS and Android
            }
        );

        document.querySelector("#orgVerifyForm").style = "";
    });

});
function sendLoginRequest(obj, auth, fromTimer) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var data = JSON.parse(this.responseText);
            var keys = Object.values(data);
            document.querySelector("#signinDiv").style = "display:none";
           // document.querySelector("#deviceready").style = "";
            document.getElementById("otp").style = "visibility: visible";
            document.getElementById("app_title").style = "visibility: visible";
            document.querySelector("#createOrg").style="right:10px;bottom:10px;position:fixed;z-index:10000;";

            $('#otp').html("");
            $('#otp').append("<div id='otps' class='otp-card'>");
            

            keys.forEach(function (k) {

                $("#otp").append("" +
                    "<div class='mdl-card mdl-card-order mdl-shadow--8dp' style='opacity:0.9;width:100%' >" +
                    "<div class='mdl-card__menu small-card' style='padding-top:25px;padding-right:25px;'>" +
                    "<div id='countdown'>" +
                    "<svg viewbox='0 0 250 250'><path class='loader' transform='translate(125, 125) scale(.60)'/></svg>" +
                    "</div>" +
                    "</div>" +
                    "<div class='mdl-card__title mdl-color--light-green-A700'>" +
                    "<h2 class='mdl-card__title-text'><b>" + k['orgName'] + "<b></h2>" +
                    "</div>" +
                    "<div class='mdl-card__supporting-text mdl-grid' >" +
                    "<div class='mdl-grid'><label class='mdl-textfield--floating-label' type='text' style='font-size:20px' id='cardholder'><p>" + k['orgDomain'] + "/" + k['orgUsername'] + "</p></label></div>" +
                    "<div class='mdl-grid'><label class='mdl-textfield--floating-label mdl-color-text--red' type='text' style='font-size:40px;' float:'right;' id='cardholder'><b>" + k['otp'] + "</b></label></div>" +
                    "</div>" +
                    "</div>"
                );


                //
                //
                //           "<div id='otps'><div class='mdl-card mdl-card-order mdl-shadow--8dp' style='opacity:0.8' >"+
                //    "<div class='mdl-card__title'>"+
                //     " <h2 class='mdl-card__title-text'>"+k['orgName']+"</h2>"+
                //      "<div id='countdown'>"+
                // "<div id='countdown-number'></div> <svg viewbox='0 0 250 250'><path class='loader' transform='translate(125, 125) scale(.98)'/></svg></div></div>"+
                // "<div class='mdl-card__supporting-text mdl-grid'  >"+
                // "<label class='mdl-textfield--floating-label' type='text' style='font-size:20px' id='cardholder'><p> "+k['orgDomain']+"/"+k['orgUsername']+"<p><b>"+ k['otp'] +"</b></p>"+"</label></div></div></div>");


            })
            $('#otp').append("</div>");
            globalObjects.serverTime = data[0]['createdTime'];
            if (!fromTimer)
                startTimer(globalObjects.serverTime, globalObjects.data, globalObjects.authKey);
        }
    };
    xhttp.open("POST", "http://192.168.43.118:8080/login/app", true);
    xhttp.setRequestHeader("Authorization", auth);
    xhttp.setRequestHeader("Content-Type", 'application/json');
    xhttp.send(obj);
}

function startTimer(serverTimestap, obj, auth) {
    var timestamp = Math.floor(new Date().getTime() / 1000);
    var timeDiff = 60 - (timestamp - serverTimestap)
        , alpha = timeDiff * 6
        , pi = Math.PI
        , timer = (1000 / 360) * 60;
    //document.write(alpha);
    //alert("BC"+alpha);
    (function draw() {
        alpha++;
        alpha %= 360; //infinite loading
        var r = (alpha * pi / 180)
            , x = Math.sin(r) * 125
            , y = Math.cos(r) * - 125
            , mid = (alpha >= 180) ? 1 : 0
            , anim = 'M 0 0 v -125 A 125 125 1 '
                + mid + ' 1 '
                + x + ' '
                + y + ' z';
        if (alpha == 357) {
            sendLoginRequest(obj, auth, true);

        }
        if (alpha < 360) {
            //clearInterval(timerId);
            setTimeout(draw, timer); // Redraw

        } else {
            anim = "M 0 0 v -125 A 125 125 1 1 1 -.1 -125 z";
        }

        for (var i = 0; i < document.getElementsByClassName('loader').length; i++)
            document.getElementsByClassName('loader')[i].setAttribute('d', anim);

    })();

}


function change() {

}

function addNewOrg(orgInfo) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log("successs");
            document.querySelector("#orgVerifyForm").style = "display:none";
            startTimer(globalObjects.serverTime, globalObjects.data, globalObjects.authKey);
        }
    };
    xhttp.open("POST", "http://192.168.43.118/users/verify/user/org", true);
    orgInfo = JSON.parse(orgInfo);
    var orgId = orgInfo["orgId"];
    var verifyCode = orgInfo["verificationCode"];
    var dataObj = {
        verificationCode: verifyCode,
        orgId: orgId
    };
    // document.querySelector("#orgId").value=JSON.stringify(dataObj);
    xhttp.setRequestHeader("Authorization", globalObjects.authKey);
    xhttp.setRequestHeader("Content-Type", 'application/json');
    xhttp.send(JSON.stringify(dataObj));
}

function login() {
    console.log("Login Called");
    var uname = document.getElementById("uname").value;
    var pass = document.getElementById("pass").value;
    this.globalObjects.username = uname;
    this.globalObjects.username = pass;
    var myKeyVals = JSON.stringify({
        username: uname
    });
    var auth = "Basic " + btoa(uname + ":" + pass);

    globalObjects.authKey = auth;
    globalObjects.data = myKeyVals;
    sendLoginRequest(myKeyVals, auth);

    // $.ajax({
    //  method: "POST",
    //  url: "http://localhost:8021/login",
    //     headers: {
    //    "Authorization": "Basic " + btoa(uname + ":" + pass)
    //  },
    //  data: myKeyVals,
    //  dataType: "application/json",
    //  success: function(data){
    //
    //
    //  }
    //});
};

function showregisterdiv() {
    console.log("showing Register div");
    //document.getElementById("deviceready").style = "visibility: hidden";
    document.getElementById("otp").style = "visibility: hidden";
    document.getElementById("app_title").style = "visibility: hidden";
    document.getElementById("registerDiv").style = "visibility: visible";
    document.getElementById("signinDiv").style = "display: none";

};
function registeruser() {
    console.log("register function called");
    var uname = document.getElementById("textfield_new_username").value;
    var pass = document.getElementById("textfield_new_password").value;
    var confirmpass = document.getElementById("textfield_password_confirm").value;
    if (pass === confirmpass) {
        this.globalObjects.username = uname;
        this.globalObjects.username = pass;
        var myKeyVals = JSON.stringify({
            username: uname
        });
        var auth = "Basic " + btoa(uname + ":" + pass);

        globalObjects.authKey = auth;
        globalObjects.data = myKeyVals;
        sendRegisterRequest(myKeyVals, auth);
    } else {
        $('#errormsg').show();
    }

};
function sendRegisterRequest(obj, auth, fromTimer) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
           // document.getElementById("deviceready").style = "visibility: visible";
            document.getElementById("otp").style = "visibility: hidden";
            document.getElementById("app_title").style = "visibility: hidden";
            document.getElementById("registerDiv").style = "visibility: hidden";

        }
    };
    xhttp.open("POST", "http://192.168.43.118/register/app", true);
    xhttp.setRequestHeader("Authorization", auth);
    xhttp.setRequestHeader("Content-Type", 'application/json');
    xhttp.send(obj);
}

function hideError() {
    $('#errormsg').hide();

}
