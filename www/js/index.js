/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var globalObjects = {};
var app = {
    // Application Constructor
    initialize: function () {
        this.bindEvents();
    },

    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener('resume', this.onResume, false);
    },

    onResume: function () {
        if (globalObjects && globalObjects.drawTimer) {
            clearTimeout(globalObjects.drawTimer);
            sendLoginRequest(globalObjects.data, globalObjects.authKey);
        }
    },
    onDeviceReady: function () {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        console.log('Received Event: ' + id);
       
        if (window.localStorage.getItem('secCode') != null) {
            $("#signRegDiv").hide();
            $("#spinner").addClass("is-active");
            globalObjects = JSON.parse(window.localStorage.getItem('secCode'));
            sendLoginRequest(globalObjects.data, globalObjects.authKey);
        } else {
            $("#spinner").removeClass("is-active");
            $("#signRegDiv").show();
        }
    }
};


$(function () {

    $(".input input").focus(function () {

        $(this).parent(".input").each(function () {
            $("label", this).css({
                "line-height": "18px",
                "font-size": "18px",
                "font-weight": "100",
                "top": "0px"
            })
            $(".spin", this).css({
                "width": "100%",
                "background": "#413c3d"
            })
        });
    }).blur(function () {
        $(".spin").css({
            "width": "100%",
            "background": "#7a7274"
        })
        if ($(this).val() == "") {
            $(this).parent(".input").each(function () {
                $("label", this).css({
                    "line-height": "60px",
                    "font-size": "24px",
                    "font-weight": "300",
                    "top": "10px"
                })
            });

        }
    });
    $("#logout_button").click(function (e) {
        window.localStorage.clear();
        document.querySelector("#signRegDiv").style = "display:block";
        document.getElementById("otp").style = "display:none";
        document.getElementById("createOrg").style = "display:none";
        if (globalObjects.drawTimer)
            clearTimeout(globalObjects.drawTimer);
        globalObjects = {};
        clearLoginBox();
        document.querySelector("#resetLogin").remove();
        document.querySelector("#loginButton").classList.remove('active');
        $("#logout_button").hide();
    });

    $("#registerbutt").click(function (e) {
        if (validateRegister()) {
            var pX = e.pageX,
                pY = e.pageY,
                oX = parseInt($(this).offset().left),
                oY = parseInt($(this).offset().top);

            $(this).append('<span id="resetLogin" class="click-efect x-' + oX + ' y-' + oY + '" style="margin-left:' + (pX - oX) + 'px;margin-top:' + (pY - oY) + 'px;"></span>')
            $('.x-' + oX + '.y-' + oY + '').animate({
                "width": "500px",
                "height": "500px",
                "top": "-250px",
                "left": "-250px",

            }, 600);
            $("button", this).addClass('active');
        }
    });
    $("#logbutt").click(function (e) {
        if (validate()) {
            var pX = e.pageX,
                pY = e.pageY,
                oX = parseInt($(this).offset().left),
                oY = parseInt($(this).offset().top);

            $(this).append('<span id="resetLogin" class="click-efect x-' + oX + ' y-' + oY + '" style="margin-left:' + (pX - oX) + 'px;margin-top:' + (pY - oY) + 'px;"></span>')
            $('.x-' + oX + '.y-' + oY + '').animate({
                "width": "500px",
                "height": "500px",
                "top": "-250px",
                "left": "-250px",

            }, 600);
            $("button", this).addClass('active');
        }
    });

    $(".alt-2").click(function (e, test) {
        clearRegisterBox();
        if (!$(this).hasClass('material-button')) {
            $(".shape").css({
                "width": "100%",
                "height": "100%",
                "transform": "rotate(0deg)"
            })

            setTimeout(function () {
                $(".overbox").css({
                    "overflow": "initial"
                })
            }, 600)

            $(this).animate({
                "width": "90px",
                "height": "90px"
            }, 500, function () {
                $(".box").removeClass("back");

                $(this).removeClass('active')
            });

            $(".overbox .title").fadeOut(300);
            $(".overbox .input").fadeOut(300);
            $(".overbox .button").fadeOut(300);

            $(".alt-2").addClass('material-buton');
        }

    })

    $(".material-button").click(function () {

        if ($(this).hasClass('material-button')) {
            setTimeout(function () {
                $(".overbox").css({
                    "overflow": "hidden"
                })
                $(".box").addClass("back");
            }, 200)
            $(this).addClass('active').animate({
                "width": "700px",
                "height": "900px"
            });

            setTimeout(function () {
                $(".shape").css({
                    "width": "50%",
                    "height": "50%",
                    "transform": "rotate(45deg)"
                })

                $(".overbox .title").fadeIn(300);
                $(".overbox .input").fadeIn(300);
                $(".overbox .button").fadeIn(300);
            }, 700)

            $(this).removeClass('material-button');

        }

        if ($(".alt-2").hasClass('material-buton')) {
            $(".alt-2").removeClass('material-buton');
            $(".alt-2").addClass('material-button');
        }

    });

});
document.addEventListener("DOMContentLoaded", function () {

    document.querySelector("#createOrg").addEventListener("click", function () {

        cordova.plugins.barcodeScanner.scan(
            function (result) {
                addNewOrg(result.text);

            },
            function (error) {
                alert("Scanning failed: " + error);
            },
            {
                preferFrontCamera: false, // iOS and Android
                showFlipCameraButton: false, // iOS and Android
                showTorchButton: false, // iOS and Android
                torchOn: false, // Android, launch with the torch switched on (if available)
                saveHistory: true, // Android, save scan history (default false)
                prompt: "Place the QR  inside the scan area", // Android
                resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
                formats: "QR_CODE", // default: all but PDF_417 and RSS_EXPANDED
                orientation: "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
                disableAnimations: true, // iOS
                disableSuccessBeep: false // iOS and Android
            }
        );

    });

});
function sendLoginRequest(obj, auth, fromTimer) {
    console.log("send login request called");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            $("#spinner").removeClass("is-active");
            $("#logout_button").show();
            var data = JSON.parse(this.responseText);
            var keys = Object.values(data);
            document.querySelector("#signRegDiv").style = "display:none";
            document.getElementById("otp").style = "visibility: visible";
            document.getElementById("app_title").style = "visibility: visible";
            document.querySelector("#createOrg").style = "right:10px;bottom:10px;position:fixed;z-index:10000;";

            $('#otp').html("");
            $('#otp').append("<div id='otps' class='otp-card'>");

            var colorArray = ["mdl-color--yellow-800", "mdl-color--yellow-900", "mdl-color--cyan-A400", "mdl-color--amber-400", "mdl-color--light-green-500"];
            var i = 5;
            keys.forEach(function (k) {
                if (i < 0)
                    i = 5;
                console.log(JSON.stringify(k));
                $("#otp").append("" +
                    "<div class='mdl-card mdl-card-order mdl-shadow--8dp mdl-cell--4-col-phone' style='opacity:0.9;width:100%;padding-left: 0px;padding-right:0px;' >" +
                    "<div class='mdl-card__menu small-card' style='padding-top:25px;padding-right:25px;'>" +
                    "<div id='countdown'>" +
                    "<svg viewbox='0 0 250 250'><path class='loader' transform='translate(125, 125) scale(.60)'/></svg>" +
                    "</div>" +
                    "</div>" +
                    "<div class='mdl-card__title " + colorArray[i] + "'>" +
                    "<h2 class='mdl-card__title-text'><b>" + k['orgName'] + "<b> <div class='timer' style='color: red;'></div></h2>" +
                    "</div>" +
                    "<div class='mdl-card__supporting-text mdl-grid' >" +
                    "<div class='mdl-grid center-items mdl-cell--4-col-phone'><label class='mdl-textfield--floating-label' type='text' style='font-size:20px' id='cardholder'><p>" + k['orgDomain'] + "/" + k['orgUsername'] + "</p></label></div>" +
                    "<div class='center-items mdl-grid mdl-cell--4-col-phone'><label class='mdl-textfield--floating-label mdl-color-text--red' type='text' style='font-size:40px;' float:'center;' id='cardholder'><b>" + k['otp'] + "</b></label></div>" +
                    "</div>" +
                    "</div>"
                );
                i--;
            })
            $('#otp').append("</div>");
            if (data.length > 0) {
                globalObjects.serverTime = data[0]['createdTime'];
                globalObjects.optCount = data.length;
                if (globalObjects.drawTimer) {
                    clearTimeout(globalObjects.drawTimer);
                }

                if (!fromTimer)
                    startTimer(globalObjects.serverTime, globalObjects.data, globalObjects.authKey);
            } else {
                globalObjects.optCount = 0;
            }
            window.localStorage.setItem('secCode', JSON.stringify(globalObjects));


        } else if (this.readyState == 4 && this.status == 500) {
            toast("Ooops Something error occured..");

            document.querySelector("#resetLogin").remove();
            document.querySelector("#loginButton").classList.remove('active');
        } else if (this.readyState == 4 && this.status == 401) {
            toast("Invalid credentials.");
            document.querySelector("#resetLogin").remove();
            document.querySelector("#loginButton").classList.remove('active');

        }
    };
    xhttp.open("POST", "http://ec2-3-16-125-247.us-east-2.compute.amazonaws.com:8080/login/app", true);
    xhttp.setRequestHeader("Authorization", auth);
    xhttp.setRequestHeader("Content-Type", 'application/json');
    xhttp.send(obj);
}

function startTimer(serverTimestap, obj, auth) {
    var timestamp = Math.floor(new Date().getTime() / 1000);
    console.log();
    var timeDiff = 60 - (timestamp - serverTimestap)
        , alpha = serverTimestap * 6
        , pi = Math.PI
        , timer = (1000 / 360) * 60;

    //document.write(alpha);
    //alert("BC"+alpha);
    (function draw() {
        //infinite loading
        var r = (alpha * pi / 180)
            , x = Math.sin(r) * 125
            , y = Math.cos(r) * -125
            , mid = (alpha >= 180) ? 1 : 0
            , anim = 'M 0 0 v -125 A 125 125 1 '
                + mid + ' 1 '
                + x + ' '
                + y + ' z';

        for (var i = 0; i < document.getElementsByClassName('loader').length; i++) {
            document.getElementsByClassName('loader')[i].setAttribute('d', anim);
            //document.getElementsByClassName("timer")[i].innerText = Math.round(alpha / 6, 0);
        }
        alpha++;
        alpha %= 360;
        if (alpha == 0) {
            console.log();
            sendLoginRequest(obj, auth);
        }
        if (alpha < 360) {
            //clearInterval(timerId);
            if (globalObjects.optCount > 0) {
                globalObjects.drawTimer = setTimeout(draw, timer);
            } else {
                clearTimeout(globalObjects.drawTimer);
            }
            // Redraw

        } else {
            anim = "M 0 0 v -125 A 125 125 1 1 1 -.1 -125 z";
        }
    })();

}

function addNewOrg(orgInfo) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log("successs");
            // document.querySelector("#orgVerifyForm").style = "display:none";
            if (globalObjects.optCount && globalObjects.optCount > 0)
                sendLoginRequest(globalObjects.data, globalObjects.authKey, true);
            else
                sendLoginRequest(globalObjects.data, globalObjects.authKey);

        } else if (this.status == 500) {

        }
    };
    xhttp.open("POST", "http://ec2-3-16-125-247.us-east-2.compute.amazonaws.com:8080/users/verify/user/org", true);
    orgInfo = JSON.parse(orgInfo);
    var orgId = orgInfo["orgId"];
    var verifyCode = orgInfo["verificationCode"];
    var dataObj = {
        verificationCode: verifyCode,
        orgId: orgId
    };
   
    xhttp.setRequestHeader("Authorization", globalObjects.authKey);
    xhttp.setRequestHeader("Content-Type", 'application/json');

    xhttp.send(JSON.stringify(dataObj));
}
function validate() {
    var uname = document.getElementById("uname").value;
    var pass = document.getElementById("pass").value;
    if (uname === "") {
        toast("Please enter username");
        return false;
    }
    if (pass === "") {
        toast("Please enter password");
        return false;
    }
    return true;
}

function validate() {
    var uname = document.getElementById("uname").value;
    var pass = document.getElementById("pass").value;
    if (uname === "") {
        toast("Please enter username");
        return false;
    }
    if (pass === "") {
        toast("Please enter password");
        return false;
    }
    return true;
}

function validateRegister() {
    var uname = document.getElementById("regname").value;
    var pass = document.getElementById("regpass").value;
    var confirmpass = document.getElementById("reregpass").value;
    var contactNumber = document.getElementById("user_contact").value;
    if (!uname) {
        toast("Please enter username");
        return false;
    }
    if (!pass) {
        toast("Please enter password");
        return false;
    }
    if (pass !== confirmpass) {
        toast("Password and Confirm Password must be same");
        return;
    }

    if (!contactNumber) {
        toast("Please enter mobile number");
        return false;
    }
    return true;
}

function clearLoginBox() {
    document.getElementById("uname").value = "";
    document.getElementById("pass").value = "";
    $("input").blur();
}

function login() {
    console.log("Login Called");
    var uname = document.getElementById("uname").value;
    var pass = document.getElementById("pass").value;
    if (uname === "") {
        return;
    }
    if (pass === "") {
        return;
    }

    this.globalObjects.username = uname;
    this.globalObjects.username = pass;
    var myKeyVals = JSON.stringify({
        username: uname
    });
    var auth = "Basic " + btoa(uname + ":" + pass);

    globalObjects.authKey = auth;
    globalObjects.data = myKeyVals;
    sendLoginRequest(myKeyVals, auth);
};

function clearRegisterBox() {
    document.getElementById("regname").value = "";
    document.getElementById("regpass").value = "";
    document.getElementById("reregpass").value = "";
    document.getElementById("user_contact").value = "";
    clearLoginBox();
}

function registeruser() {
    console.log("register function called");
    var uname = document.getElementById("regname").value;
    var pass = document.getElementById("regpass").value;
    var confirmpass = document.getElementById("reregpass").value;
    var contactNumber = document.getElementById("user_contact").value;
    if (!uname) {
        return;
    }
    if (!pass) {
        return;
    }
    if (pass !== confirmpass) {
        return;
    }

    if (!contactNumber) {
        return;
    }

    var myKeyVals = JSON.stringify({
        userName: uname,
        passcode: pass,
        contact: contactNumber
    });
    sendRegisterRequest(myKeyVals);
};

function sendRegisterRequest(obj) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 201) {
            document.getElementById("otp").style = "display: none";
            toast("User created successfully", "green");
            document.querySelector("#crossRegisterButton").click();

        } else if (this.readyState == 4 && this.status == 409) {
            console.log("user already exists");
            toast("User already exists");
        } else if (this.readyState == 4 && this.status == 500) {
            console.log("user already exists");
            toast("Ooops Something error occured..");
        }
    };
    xhttp.open("POST", "http://ec2-3-16-125-247.us-east-2.compute.amazonaws.com:8080/users", true);
    xhttp.setRequestHeader("Content-Type", 'application/json');
    xhttp.send(obj);
}

function toast(message, color) {
    var snackbarContainer = document.querySelector('#toast');
    var data1 = {
        message: message,
        timeout: 3000
    };

    snackbarContainer.style.alignContent = "center";
    snackbarContainer.classList.remove('mdl-color--green-700');
    snackbarContainer.classList.remove('mdl-color--red-700');
    if (color) {
        snackbarContainer.classList.add('mdl-color--green-700');
    } else {
        snackbarContainer.classList.add('mdl-color--red-700');
    }
    snackbarContainer.MaterialSnackbar.showSnackbar(data1);
}